//
//  DetailPresenterTests.swift
//  weatherAppTests
//
//  Created by Rober Martinez Bejarano on 24/03/23.
//

import XCTest
@testable import weatherApp

@MainActor
class DetailPresenterTests: XCTestCase {

  var presenter : DetailPresenter!
  var presenterDelegate : DetailViewMock!
  var providerMock : DetailProviderMock!
  var timeOut : TimeInterval = 2.0
  
  @MainActor
  override func setUpWithError() throws {
    MockManager.shared.runAppWithMock = true
    presenterDelegate = DetailViewMock()
    providerMock = DetailProviderMock()
    presenter = DetailPresenter(delegate: presenterDelegate, provider: providerMock)
  }
  
  @MainActor
  override func tearDownWithError() throws {
    presenter = nil
    presenterDelegate = nil
    providerMock = nil
  }
  
  func testGetCities_Should_get_the_weather_information() {
    presenterDelegate.expGetInfoWasCalled = expectation(description: "Loading weather information")
    Task {
      await presenter.getWeatherInformation(cityId: 1, lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertNotNil(presenter.weatherObj)
    XCTAssertTrue(presenterDelegate.getInfoWasCalled)
  }
  
  func testGetCities_Should_catch_error() {
    MockManager.shared.runAppWithMock = false
    presenterDelegate = DetailViewMock()
    providerMock = DetailProviderMock()
    providerMock.throwError = true
    presenter = DetailPresenter(delegate: presenterDelegate, provider: providerMock)

    presenterDelegate.expShowError = expectation(description: "Loading weather information")
    Task {
      await presenter.getWeatherInformation(cityId: 1, lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertTrue(presenterDelegate.showErrorWasCalled)
    XCTAssertFalse(presenterDelegate.getInfoWasCalled)
  }
  

  func testGetWeatherInformation_Should_show_and_hide_loading() {
    presenterDelegate.expLoading = expectation(description: "show/hide loading")
    presenterDelegate.expLoading?.expectedFulfillmentCount = 2
    
    Task {
      await presenter.getWeatherInformation(cityId: 1, lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertTrue(presenterDelegate.loadingViewWasCalled)
    XCTAssertTrue(presenterDelegate.loadingShow)
    XCTAssertTrue(presenterDelegate.loadingHide)
  }
}

class DetailViewMock : DetailViewProtocol {
  var loadingViewWasCalled : Bool = false
  var loadingShow : Bool = false
  var loadingHide : Bool = false
  var showErrorWasCalled : Bool = false
  var getInfoWasCalled : Bool = false
  var expGetInfoWasCalled : XCTestExpectation?
  var expLoading : XCTestExpectation?
  var expShowError : XCTestExpectation?
  
  func loadingView(_ state: weatherApp.LoadingViewState) {
    loadingViewWasCalled = true
    if state == .show {
      loadingShow = true
    } else {
      loadingHide = true
    }
    expLoading?.fulfill()
  }
  
  func showError(_ error: String, callback: (() -> Void)?) {
    showErrorWasCalled = true
    expShowError?.fulfill()
  }
  
  func getInfo(weather: weatherApp.WeatherModel) {
    getInfoWasCalled = true
    expGetInfoWasCalled?.fulfill()
  }
}
