//
//  ViewControllerTests.swift
//  weatherAppTests
//
//  Created by Rober Martinez Bejarano on 24/03/23.
//

import XCTest
@testable import weatherApp

class ViewControllerTests: XCTestCase {
  
  var controller: ViewController!
  
  override func setUpWithError() throws {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(identifier: "MainViewController") as! ViewController
    controller = vc
    controller.loadViewIfNeeded()
  }
  
  override func tearDownWithError() throws {
    controller = nil
  }
  
  func testLoadViewController() throws {
    let tableView = try XCTUnwrap(controller.tableViewMain, "should create this IBOutlet")
    XCTAssertNotNil(tableView)
  }
}
