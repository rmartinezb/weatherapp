//
//  MainPresenterTests.swift
//  weatherAppTests
//
//  Created by Rober Martinez Bejarano on 24/03/23.
//

import XCTest
@testable import weatherApp

@MainActor
final class MainPresenterTests: XCTestCase {
  
  var presenter : MainPresenter!
  var presenterDelegate : MainViewMock!
  var providerMock : MainProviderMock!
  var timeOut : TimeInterval = 2.0
  
  @MainActor
  override func setUpWithError() throws {
    MockManager.shared.runAppWithMock = true
    presenterDelegate = MainViewMock()
    providerMock = MainProviderMock()
    presenter = MainPresenter(delegate: presenterDelegate, provider: providerMock)
  }
  
  @MainActor
  override func tearDownWithError() throws {
    presenter = nil
    presenterDelegate = nil
    providerMock = nil
  }
  
  func testGetCities_Should_get_the_list_of_cities() {
    presenterDelegate.expGetDataWasCalled = expectation(description: "Loading data cities")
    Task {
      await presenter.getCities(lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertTrue(presenter.citiesList.count > 0)
    XCTAssertTrue(presenterDelegate.getDataWasCalled)
  }
  
  func testGetCities_Should_catch_error() {
    MockManager.shared.runAppWithMock = false
    presenterDelegate = MainViewMock()
    providerMock = MainProviderMock()
    providerMock.throwError = true
    presenter = MainPresenter(delegate: presenterDelegate, provider: providerMock)
    
    presenterDelegate.expShowError = expectation(description: "Loading data cities")
    Task {
      await presenter.getCities(lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertTrue(presenterDelegate.showErrorWasCalled)
    XCTAssertFalse(presenterDelegate.getDataWasCalled)
  }
  
  func testGetCities_Should_show_and_hide_loading() {
    presenterDelegate.expLoading = expectation(description: "show/hide loading")
    presenterDelegate.expLoading?.expectedFulfillmentCount = 2
    
    Task {
      await presenter.getCities(lat: 1.1, lon: 2.2)
    }
    
    waitForExpectations(timeout: timeOut)
    
    XCTAssertTrue(presenterDelegate.loadingViewWasCalled)
    XCTAssertTrue(presenterDelegate.loadingShow)
    XCTAssertTrue(presenterDelegate.loadingHide)
  }
}

class MainViewMock : MainViewProtocol {
  var loadingViewWasCalled : Bool = false
  var loadingShow : Bool = false
  var loadingHide : Bool = false
  var showErrorWasCalled : Bool = false
  var getDataWasCalled : Bool = false
  var expGetDataWasCalled : XCTestExpectation?
  var expLoading : XCTestExpectation?
  var expShowError : XCTestExpectation?
  
  func loadingView(_ state: weatherApp.LoadingViewState) {
    loadingViewWasCalled = true
    if state == .show {
      loadingShow = true
    } else {
      loadingHide = true
    }
    expLoading?.fulfill()
  }
  
  func showError(_ error: String, callback: (() -> Void)?) {
    showErrorWasCalled = true
    expShowError?.fulfill()
  }
  
  func getData(list: [weatherApp.City]) {
    getDataWasCalled = true
    expGetDataWasCalled?.fulfill()
  }
}
