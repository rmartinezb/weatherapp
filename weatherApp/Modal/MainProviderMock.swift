//
//  MainProviderMock.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import Foundation

class MainProviderMock : MainProviderProtocol {
  var throwError : Bool = false
  
  func getCities(lat: Double, lon: Double) async throws -> [City] {
    if throwError {
      throw NetworkError.httpResponseError
    }
    
    guard let model = Utils.parseJson(jsonName: "Cities", model: CityModel.self) else {
      throw NetworkError.jsonDecoder
    }
    
    var cities = model.cities

    if lat != 0.0 {
      let actual = City(id: 1, name: "Current Location",state: "",country: "CO",coord: Coordinate(lat: lat, lon: lon))
      cities.append(actual)
    }
    
    return cities
  }
}
