//
//  DetailProviderMock.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import Foundation

class DetailProviderMock : DetailProviderProtocol {
  var throwError : Bool = false
    
  func getWeatherInformation(id: Int, lat: Double, lon: Double) async throws -> WeatherModel {
    if throwError {
      throw NetworkError.httpResponseError
    }
    
    guard let model = Utils.parseJson(jsonName: "Weather", model: WeatherModel.self) else {
      throw NetworkError.jsonDecoder
    }
    
    do {
      try UserDefaults.standard.setObject(model, forKey: String(model.id))
    } catch {
      print("Error UserDefaults: \(error.localizedDescription)")
    }
    
    return model
  }
}
