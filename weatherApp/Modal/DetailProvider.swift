//
//  DetailProvider.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import Foundation

protocol DetailProviderProtocol {
  func getWeatherInformation(id: Int, lat: Double, lon: Double) async throws -> WeatherModel
}

class DetailProvider : DetailProviderProtocol {
  func getWeatherInformation(id: Int, lat: Double, lon: Double) async throws -> WeatherModel {
    let queryParams : [String:String] = ["lat":"\(lat)", "lon":"\(lon)", "appid": Constants.apiKey, "units": "metric"]
    let requestModel = RequestModel(endpoint: .empty, queryItems: queryParams)
    
    do {
      let model = try await ServiceLayer.callService(requestModel, WeatherModel.self)
      if id == 1 {
        try UserDefaults.standard.setObject(model, forKey: "1")
      } else {
        try UserDefaults.standard.setObject(model, forKey: String(model.id))
      }
      return model
    } catch {
      throw error
    }
  }
}
