//
//  CityModel.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation

struct CityModel: Decodable {
  let cities: [City]
}

struct City: Decodable {
  let id: Int
  let name, state, country: String
  let coord: Coordinate
}

struct Coordinate: Decodable {
  let lat, lon: Double
}
