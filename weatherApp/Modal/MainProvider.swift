//
//  MainProvider.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation

protocol MainProviderProtocol {
  func getCities(lat: Double, lon: Double) async throws -> [City]
}

class MainProvider : MainProviderProtocol {
  func getCities(lat: Double, lon: Double) async throws -> [City] {
    let bogota = City(id: 3688689, name: "Bogotá",state: "",country: "CO",coord: Coordinate(lat: 4.60971, lon: -74.081749))
    let cali = City(id: 3687925, name: "Cali",state: "",country: "CO",coord: Coordinate(lat: 3.43722, lon: -76.522499))
    let barranquilla = City(id: 3689147, name: "Barranquilla",state: "",country: "CO",coord: Coordinate(lat: 10.96389, lon: -74.796387))
    
    if lat == 0.0 {
      return [bogota, cali, barranquilla]
    } else {
      let actual = City(id: 1, name: "Current Location",state: "ACTUAL",country: "CO",coord: Coordinate(lat: lat, lon: lon))
      return [bogota, cali, barranquilla, actual]
    }
  }
}
