//
//  WeatherModel.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import Foundation

struct WeatherModel: Codable {
  let id, dt: Int
  let name: String
  let weather: [Weather]
  let main: Main
  let wind: Wind
  let sys: Sys
}

struct Weather: Codable {
  let id: Int
  let main, description, icon: String
}

struct Main: Codable {
  let temp, feelsLike, tempMin, tempMax: Double
  let pressure, humidity: Int
  
  enum CodingKeys: String, CodingKey {
    case temp, pressure, humidity
    case feelsLike = "feels_like"
    case tempMin = "temp_min"
    case tempMax = "temp_max"
  }
}

struct Wind: Codable {
  let speed: Double
  let deg: Int
}

struct Sys: Codable {
  let country: String
  let sunrise, sunset: Int
}
