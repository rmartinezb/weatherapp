//
//  RequestModel.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation

struct RequestModel {
  let endpoint : Endpoints
  var queryItems : [String:String]?
  let httpMethod : HttpMethod = .GET
  var baseUrl : URLBase = .weather
  
  func getURL() -> String {
    return baseUrl.rawValue + endpoint.rawValue
  }
  
  enum HttpMethod : String {
    case GET
    case POST
  }
  
  enum Endpoints : String {
    case info = "/info"
    case empty = ""
  }
  
  enum URLBase : String {
    case weather = "https://api.openweathermap.org/data/2.5/weather"
    case other = "other"
  }
}
