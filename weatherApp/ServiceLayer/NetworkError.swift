//
//  NetworkError.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation

enum NetworkError: String, Error {
  case invalidURL
  case couldNotDecodeData
  case httpResponseError
  case statusCodeError
  case jsonDecoder
  case unauthorized
}

extension NetworkError : LocalizedError {
  public var errorDescription: String? {
    switch self {
      case .invalidURL:
        return NSLocalizedString("The URL is invalid.", comment: "")
      case .couldNotDecodeData:
        return NSLocalizedString("Not decode the data.", comment: "")
      case .httpResponseError:
        return NSLocalizedString("Unable to get the HTTPURLResponse.", comment: "")
      case .statusCodeError:
        return NSLocalizedString("This status code is different from 200.", comment: "")
      case .jsonDecoder:
        return NSLocalizedString("Failed to read the JSON and could not decode.", comment: "")
      case .unauthorized:
        return NSLocalizedString("The session ended.", comment: "")
    }
  }
}
