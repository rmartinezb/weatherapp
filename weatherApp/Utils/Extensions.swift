//
//  Constants.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation
import UIKit

extension UIApplication {
  var statusBarHeight: CGFloat {
    connectedScenes
      .compactMap {
        $0 as? UIWindowScene
      }
      .compactMap {
        $0.statusBarManager
      }
      .map {
        $0.statusBarFrame
      }
      .map(\.height)
      .max() ?? 0
  }
}


extension Date {
  func unixToString(_ time: Int, _ format: String = "HH:mm") -> String {
    let date = Date(timeIntervalSince1970: TimeInterval(time))
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    
    return dateFormatter.string(from: date)
  }
}


protocol ObjectSavable {
  func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable
  func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable
}

extension UserDefaults: ObjectSavable {
  func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
    let encoder = JSONEncoder()
    do {
      let data = try encoder.encode(object)
      set(data, forKey: forKey)
    } catch {
      throw ObjectSavableError.unableToEncode
    }
  }
  
  func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable {
    guard let data = data(forKey: forKey) else { throw ObjectSavableError.noValue }
    let decoder = JSONDecoder()
    do {
      let object = try decoder.decode(type, from: data)
      return object
    } catch {
      throw ObjectSavableError.unableToDecode
    }
  }
}

enum ObjectSavableError: String, LocalizedError {
  case unableToEncode = "Unable to encode object into data"
  case noValue = "No data object found for the given key"
  case unableToDecode = "Unable to decode object into given type"
  
  var errorDescription: String? {
    rawValue
  }
}
