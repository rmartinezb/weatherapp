//
//  BaseViewController.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import UIKit

enum LoadingViewState {
  case show
  case hide
}

protocol BaseViewProtocol {
  func loadingView(_ state : LoadingViewState)
  func showError(_ error : String, callback : (()->Void)?)
}

class BaseViewController: UIViewController {
  var loadingIndicator = UIActivityIndicatorView(style: .large)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let statusBarHeight = UIApplication.shared.statusBarHeight
    let statusbarView = UIView()
    statusbarView.backgroundColor = UIColor.systemIndigo
    view.addSubview(statusbarView)
    
    statusbarView.translatesAutoresizingMaskIntoConstraints = false
    statusbarView.heightAnchor
      .constraint(equalToConstant: statusBarHeight).isActive = true
    statusbarView.widthAnchor
      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
    statusbarView.topAnchor
      .constraint(equalTo: view.topAnchor).isActive = true
    statusbarView.centerXAnchor
      .constraint(equalTo: view.centerXAnchor).isActive = true
  }
  
  func showError(_ error : String, callback : (()->Void)?) {
    let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
    
    if let callback = callback{
      alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
        if action.style == .default {
          callback()
        }
      }))
    }
    
    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
      if action.style == .cancel {
        print("ok button pressed")
      }
    }))
    
    present(alert, animated: true)
  }
  
  func loadingView(_ state : LoadingViewState) {
    switch state {
      case .show:
        showLoading()
      case .hide:
        hideLoading()
    }
  }
  
  private func showLoading() {
    view.addSubview(loadingIndicator)
    loadingIndicator.center = view.center
    loadingIndicator.startAnimating()
  }
  
  private func hideLoading() {
    loadingIndicator.stopAnimating()
    loadingIndicator.removeFromSuperview()
  }
}
