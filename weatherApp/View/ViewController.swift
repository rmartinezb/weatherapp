//
//  ViewController.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import UIKit
import CoreLocation

class ViewController : BaseViewController {
  
  @IBOutlet weak var tableViewMain: UITableView!
  
  let locationManager = CLLocationManager()
  lazy var presenter = MainPresenter(delegate: self)
  private var cities: [City] = []
  private var city: City?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    locationManager.delegate = self
    tableViewMain.delegate = self
    tableViewMain.dataSource = self
    loadingView(.show)
    
    switch locationManager.authorizationStatus {
      case .notDetermined:
        locationManager.requestWhenInUseAuthorization()
      case .denied:
        Task {
          await presenter.getCities(lat: 0, lon: 0)
        }
      default:
        locationManager.requestLocation()
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
    let destinationVC = segue.destination as! DetailViewController
    destinationVC.city = city
  }
}

extension ViewController : MainViewProtocol {
  func getData(list: [City]) {
    cities = list
    self.tableViewMain.reloadData()
  }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellMain", for: indexPath) as! MainCell
    let city = cities[indexPath.row]
    cell.nameLabel.text = city.name
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    city = cities[indexPath.row]
    performSegue(withIdentifier: "detailSegue", sender: nil)
  }
}

extension ViewController : CLLocationManagerDelegate {
  func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {

    if (manager.authorizationStatus == .denied) {
      Task {
        await presenter.getCities(lat: 0, lon: 0)
      }
    } else {
      locationManager.requestLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.first {
      let latitude = location.coordinate.latitude
      let longitude = location.coordinate.longitude

      Task {
        await presenter.getCities(lat: latitude, lon: longitude)
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Error location: \(error.localizedDescription)")
    loadingView(.hide)
  }
}
