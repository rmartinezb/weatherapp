//
//  DetailViewController.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import UIKit

class DetailViewController: BaseViewController {
  
  @IBOutlet weak var cityName: UILabel!
  @IBOutlet weak var country: UILabel!
  @IBOutlet weak var weatherImage: UIImageView!
  @IBOutlet weak var temperature: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var thermalSensation: UILabel!
  @IBOutlet weak var humidity: UILabel!
  @IBOutlet weak var tempMin: UILabel!
  @IBOutlet weak var tempMax: UILabel!
  @IBOutlet weak var wind: UILabel!
  @IBOutlet weak var sunrise: UILabel!
  @IBOutlet weak var sunset: UILabel!
  @IBOutlet weak var containerView: UIStackView!
  
  lazy var presenter = DetailPresenter(delegate: self)
  var city: City!
  var weatherResp: WeatherModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
        
    containerView.isHidden = true
    Task {
      await presenter.getWeatherInformation(cityId: city.id, lat: city.coord.lat, lon: city.coord.lon)
    }
  }
  
  func setUI(response: WeatherModel) {
    cityName.text = response.name
    country.text = response.sys.country
    temperature.text = "\(Int(response.main.temp))° C"
    descriptionLabel.text = response.weather[0].description.uppercased()
    humidity.text = "\(response.main.humidity)%"
    tempMin.text = "\(Int(response.main.tempMin))° C"
    tempMax.text = "\(Int(response.main.tempMax))° C"
    wind.text = "\(Int(response.wind.speed)) m/s"
    thermalSensation.text = "The thermal sensation is \(Int(response.main.feelsLike))° C"
    sunrise.text = Date.init().unixToString(response.sys.sunrise)
    sunset.text = Date.init().unixToString(response.sys.sunset)
    weatherImage.image = UIImage(named: response.weather[0].icon)
    containerView.isHidden = false
  }
}

extension DetailViewController : DetailViewProtocol {
  func getInfo(weather: WeatherModel) {
    setUI(response: weather)
  }
}
