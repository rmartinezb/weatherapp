//
//  DetailPresenter.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 22/03/23.
//

import Foundation

protocol DetailViewProtocol: AnyObject, BaseViewProtocol {
  func getInfo(weather : WeatherModel)
}

@MainActor
class DetailPresenter {
  var provider : DetailProviderProtocol
  weak var delegate : DetailViewProtocol?
  var weatherObj: WeatherModel?

  init(delegate : DetailViewProtocol, provider : DetailProviderProtocol = DetailProvider()) {
    self.provider = provider
    self.delegate = delegate
    
#if DEBUG
    if MockManager.shared.runAppWithMock {
      self.provider = DetailProviderMock()
    }
#endif
  }
  
  func getWeatherInformation(cityId: Int, lat: Double, lon: Double) async {
    delegate?.loadingView(.show)
    
    if(InternetConnectionManager.isConnectedToNetwork()) {
      do {
        defer {
          delegate?.loadingView(.hide)
        }
        
        weatherObj = nil
        let weather = try await provider.getWeatherInformation(id: cityId, lat: lat, lon: lon)
        weatherObj = weather
        delegate?.getInfo(weather: weather)
      } catch {
        delegate?.showError(error.localizedDescription, callback: {
          Task {[weak self] in
            await self?.getWeatherInformation(cityId: cityId, lat:lat, lon:lon)
          }
        })
      }
    } else {
      do {
        defer {
          delegate?.loadingView(.hide)
        }
        
        weatherObj = nil
        let weather = try UserDefaults.standard.getObject(forKey: String(cityId), castTo: WeatherModel.self)
        weatherObj = weather
        delegate?.getInfo(weather: weather)
      } catch {
        delegate?.showError("You do not have internet and no information was found for the selected city", callback: {
          Task {[weak self] in
            await self?.getWeatherInformation(cityId: cityId, lat:lat, lon:lon)
          }
        })
      }
    }
  }
}
