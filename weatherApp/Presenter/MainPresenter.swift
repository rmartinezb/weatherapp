//
//  MainPresenter.swift
//  weatherApp
//
//  Created by Rober Martinez Bejarano on 21/03/23.
//

import Foundation

protocol MainViewProtocol: AnyObject, BaseViewProtocol {
  func getData(list : [City])
}

@MainActor
class MainPresenter {
  var provider : MainProviderProtocol
  weak var delegate : MainViewProtocol?
  var citiesList = [City]()
  
  init(delegate : MainViewProtocol, provider : MainProviderProtocol = MainProvider()) {
    self.provider = provider
    self.delegate = delegate
    
#if DEBUG
    if MockManager.shared.runAppWithMock {
      self.provider = MainProviderMock()
    }
#endif
  }
  
  func getCities(lat: Double, lon: Double) async {
    delegate?.loadingView(.show)
    
    do {
      defer {
        delegate?.loadingView(.hide)
      }
      
      citiesList.removeAll()
      let cities = try await provider.getCities(lat: lat, lon: lon)
      citiesList = cities
      delegate?.getData(list: cities)
    } catch {
      delegate?.showError(error.localizedDescription, callback: nil)
    }
  }
}
